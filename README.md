# fedoratorrent

## Overview
Script to keep the latest Fedora Respin torrents up to date and available via Transmission.

## Fedora Respins
If you are not aware, the Fedora Project creates a Gold release of its supported ISOs and for most releases these ISOs are never updated.  Fedora is a fast moving distribution and receives a lot of updates, including those to the installer, kernel, drivers, as well other areas.  Using a Respin will allow you to update with the latest software as of the generation of the ISO which allows users to use the latest kernel and reduce the number of updates to download after installation.

## Script setup
Update the script with the paths that you have set in your Transmission client and the authorization you have set up.  Make sure the paths are correct and Transmission is set to never stop seeding.

In order to get the passwords out of the script for source control, I have put a sample file, transauth.sh which by default in the script should be in $HOME/.transauth.sh and you put your authentication in that script and it will use it rather than editing the main script itself.

## How to mix personal torrent usage with the Fedora torrents
If you use Transmission for other torrents, you can change their seeding ratios etc as they are downloaded or possibly use a separate client.

Thank you for helping seed Fedora Respins!
