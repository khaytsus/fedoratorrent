#!/bin/bash

# Script to seed the very latest torrent files from a directory.  Once new
# ones are in place the old torrents are completely removed.  If you want
# to save the old torrents you could add that before the torrent and data
# is removed, but I consider only the latest torrents useful.

# Uses Transmission, but any torrent client with some CLI interface would
# work, just needs to be able to remove the torrents when done so it can get
# rid of the old torrent references, data, etc.

# URL of torrents to seed
site="dl.fedoraproject.org/pub/alt/live-respins/"

# Drive paths; no spaces or special characters please

# Path to pull in the latest torrent files
latest="/movies/fedora-respins/latest/"
# Path to store the current in-use torrent files for comparison
current="/movies/fedora-respins/current/"
# Path to move torrent files to be picked up by Transmission
in="/movies/torrents-in/"
# Path where the iso files themselves go
datain="/movies/torrents/"

# If this is 1, pull the iso's too
getiso="1"

# Our config file, to get passwords out of the script
config="${HOME}/.transauth.sh"

# Transmission authorization info
if [ -e ${config} ]; then
    source ${config}
else
    tauth="user:pass"
fi

# Transmission stats log for giggles
log="/movies/fedora-respins/stats.log"

# Torrent file pattern so we only list/remove torrents that match this pattern
tpattern="F30"

# Common log function
log ()
{
    line=$*
    date=`date`
    echo "${date} - ${line}" | tee -a ${log}
}

log "Starting $0"

# Make sure the directory is cleaned up
rm -fR ${latest}
mkdir -p ${latest}

cd ${latest}

# Clean up old files
rm -f *.torrent

# Download the latest torrent files
wget --quiet -r -l1 --no-parent -A .torrent https://${site}

# Move torrents to current directory and clean up full path
mv ${site}/*.torrent .
rmdir -p ${site}

# Compare the two directories to see if these files are new
diff ${latest} ${current} &> /dev/null
rc=$?

if [ ${rc} == 1 ]; then
    log "Files differ, copying new torrent files"
    rm -f ${current}/*.torrent
    # Copy the latest files so we know when the source files update next time
    cp ${latest}/*.torrent ${current}

    # Now, grab the iso files too if $getiso is set to 1
    if [ ${getiso} == 1 ]; then
        log "getiso set to ${getiso}, downloading isos"
        wget -r -l1 --no-parent -A .iso -R "${tpattern}-source*.iso" https://${site}
        mv ${site}/*.iso ${datain}
        rmdir -p ${site}
    else
        log "getiso set to ${getiso}, not downloading isos"
    fi

    # Iterate through the torrents and remove each one that matches our pattern
    # from both Transmission and the disk

    torid="init"
    maxloops="20"
    counter="0"

    while [ "${torid}" != "" ]; do
        torid=`transmission-remote -n ${tauth} -l | grep ${tpattern} | \
               head -1 | sed -e 's/^ *//g' -e 's/  */ /g' | cut -f 1 -d " "`
        # Transmission puts a * after active torrents, strip that out
        torid=$(echo ${torid} | sed 's/[^0-9]*//g')
        # Make sure we actually got an id
        if [ "${torid}" != "" ]; then
            # Collect stats in logs to see how much it was used before cleanup
            log "Torrents have been updated; dumping stats before removing old ones"
            transmission-remote -n ${tauth} -t ${torid} -i >> ${log}

            # Remove torrent and its data
            log "Removing torrent id: [${torid}]"
            transmission-remote -n ${tauth} -t ${torid} -rad >> ${log}
        fi
        # Sanity check; if we're looping, bail out after $maxloops
        ((counter++))
        if [ ${counter} -gt ${maxloops} ]; then
            torid=""
            log "Had to bail out of loop; something wrong?"
        fi
    done

    # Copy the latest torrent files into Transmissions in folder for it to use
    cp ${latest}/*.torrent ${in}
else
    # Torrents are up to date, just log latest stats
    log "Torrents are up to date, dumping individual stats"
    transmission-remote -n ${tauth} -t all -i >> ${log}
    log "Torrents are up to date, dumping overall stats"
    transmission-remote -n ${tauth} -st >> ${log}
fi

log "Finished $0"